FROM httpd
LABEL version=1.0.0
RUN apt update
RUN apt install -y git
RUN apt install -y nano
RUN rm -rf /usr/local/apache2/htdocs
RUN mkdir /usr/local/apache2/htdocs
RUN git clone https://gitlab.com/peguie-keutcha/siteweb.git /usr/local/apache2/htdocs

#Ajouter des arguments
ARG site_title
ARG nom
ARG prenom
ARG image-url
ARG path=/usr/local/apache2/htdocs/index.html
RUN sed -i "s/{{SITE_TITLE}}/${site_title}/g" ${path}
RUN sed -i "s/{{NOM}}/${nom}/g" ${path}
RUN sed -i "s/{{PRENOM}}/${prenom}/g" ${path}
RUN sed -i "s/{{IMAGE_URL}}/${image_url}/g" ${path}
EXPOSE 80
